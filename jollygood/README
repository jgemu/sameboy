SameBoy
-------
SameBoy is an emulator for the Nintendo Game Boy/Game Boy Color.

Source for the emulator core is unmodified from upstream sources.

Special Build Requirements
--------------------------
SameBoy requires the Rednex Game Boy Development System to build BIOS images:
  https://github.com/gbdev/rgbds (v0.6.0 or later)

Compiling
---------
Make sure you have The Jolly Good API's header files installed. If you did
not install them, you will be required to include their path in CPPFLAGS.

Options:
  DISABLE_MODULE - Set to a non-zero value to disable building the module.
  ENABLE_STATIC_JG - Set to a non-zero value to build a static JG archive.

Linux:
  make

macOS:
  make

BSD:
  gmake

Windows (MSYS2):
  make

Cross Compile:
(For example compiling on Linux for MinGW)
  AR=x86_64-w64-mingw32-ar \
  CC=x86_64-w64-mingw32-cc \
  CC_FOR_BUILD=x86_64-pc-linux-gnu-cc \
  PKG_CONFIG=x86_64-w64-mingw32-pkg-config \
  STRIP=x86_64-w64-mingw32-strip \
  make

The build will be output to "sameboy/". This directory may be used as is
locally by copying it to your local "cores" directory, or may be installed
system-wide using the "install" target specified in the Makefile.

Auxiliary Files
---------------
SameBoy supports "2 Player Mode" via Auxiliary Files. The Auxiliary File will
be loaded as the second game (the first game specified on the command line in
The Jolly Good Reference Frontend, and displayed on the bottom). Save data
will work for both games, but cheats and states only work for the main game.
The link cable and IR sensor are always considered connected in 2 Player Mode.

Settings
--------
bios_external = 0
0 = Prefer Internal BIOS, 1 = Prefer External BIOS

color_correction = 2
0 = Disabled, 1 = Correct Color Curves, 2 = Modern - Balanced,
3 = Modern - Boost Contrast, 4 = Reduce Contrast, 5 = Harsh Reality
6 = Modern - Accurate

highpass_filter = 1
0 = Off, 1 = Accurate, 2 = DC Offset

model = 1
0 = Game Boy, 1 = Game Boy Color, 2 = Game Boy Advance, 3 = Super Game Boy,
4 = Super Game Boy 2

revision_gbc = 4
0 = CGB, 1 = CGB A, 2 = CGB B, 3 = CGB C, 4 = CGB D, 5 = CGB E

rumble = 1
0 = Disabled, 1 = Rumble Games, 2 = All Games

turbo_rate = 3
N = Pulse every N frames
